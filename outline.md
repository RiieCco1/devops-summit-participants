# Course outline


## Stage 1: Getting started

First, go to https://gitlab.com and create an account there if you do not already have one.


#### After that?

You come by the table, you give us your handle and we'll add you to the gitlab repo.
There, you'll get a project engagement ID you can use in the Defect Dojo environment, along with the namespace you can deploy your security pods to.

#### Go to gitlab
Go to the project we just gave you access to and create a new branch for yourself.

## Stage 2 Run ZAP

We first start by configuring the owasp ZAP scan to our needs.
This first ZAP scan is a passive scan. This doesn't do any invasive attacks. Instead, it only
checks really quickly for basic configuration errors and other low-hanging fruit.

You can login into Defect Dojo on https://defect-dojo.azurewebsites.net with the following credentials:

```
User: DevOps2018
Pass: DevOps2018!
```

You need Defect Dojo to congifure your ZAP scan, since you need to get the API key and
feed it to the owasp-zap-secrets.yaml file. Otherwise your scan wil not be able to 
push the metrics to Defect Dojo. *Note:* Everything you use as values for the secrets file has to be base 64 encoded.

After configuring your scan properly you can commit your changes. Gitlab will instantly
start a build and you can follow that through the Gitlab interface.

```
TARGET: http://flask-hacking.azurewebsites.net/
```

## Stage 3: NPM-AUDIT

In this stage we are going to configure our own scan from start to finish.
You can use everything provided by the ZAP scan as a baseline of doing so.

In this stage we do not yet have to build containers ourselves. You can simply
pick the workshop image we pre-built for this workshop from.

```
rtencatexebia/npm-audit
```

For more detailed information about what parameters the scan expects you can check `npm-audit-guide.md`
In the root directory of this workshop repo.

The repository we are going to scan with this npm-audit container is the following source:

```
https://github.com/bkimminich/juice-shop.git
```

We need the results from this scan for later hacking! :-)

## Stage 4: Bandit

For this stage we are going to build and test our own image locally.
After building this image you may push it to your own registry if you like.

If you do not have one and do not want to create it, no worries.

Show us your image works and then for your pipeline you can utilize this image:

```
xebiasecurity/bandit-defectdojo
```

the repository we want to scan is the following:

```
https://github.com/riiecco/assessment
```

#### Using the metrics
Now, we are going to take the metrics and look into Defect dojo.

We are going to do active validation on the findings presented there.
We are going to do this by cloning the repo

```
https://github.com/riiecco/assessment
```

And deploying and hacking this image locally


CD to the directory after cloning than run:

```
docker build -t <name> .
```

After you have built it, run:

```
docker run -p80:80 <name>
```

Or pull the image directly from the docker hub and run it:

```
docker pull rtencatexebia/flask-hacking-demo
docker run -p80:80 rtencatexebia/flask-hacking-demo
```

If for some reason you can not run docker, go to:

```
http://flask-hacking.azurewebsites.net/
```

## Stage 5: Manual import and finding verification

Now we are going to upload our own scan to Defect Dojo manually. You can find the juice-shop2.xml file in the master repo.
Don't forget, dojo is running on https://defect-dojo.azurewebsites.net.

Take a look at the findings. Run the web app under investigation with `docker run --rm -p 3000:3000 bkimminich/juice-shop`.

If you cannot run the image locally, use https://juice-shop.herokuapp.com.

Go ahead and try to do some testing! If you find anything you can't reproduce, or rather, security issues not listed - by all means fix it :)